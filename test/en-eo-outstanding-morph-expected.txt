[-CRnDC-Tcui7#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^they/prpers<prn><subj><p3><mf><pl>$ ^saw/saw<n><sg>/see<vblex><past>$
[/-CRnDC-Tcui7]
[-_SaBUhTxLzV#0] ^They/Prpers<prn><subj><p3><mf><pl>$ ^exploit/exploit<n><sg>/exploit<vblex><inf>/exploit<vblex><pres>$ ^all/all<predet><sp>/all<prn><tn><sp>$ ^of/of<pr>$ ^humanity/humanity<n><sg>$
[/-_SaBUhTxLzV]
[-t9KC8OtnpLB#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^she/prpers<prn><subj><p3><f><sg>$ ^saw/saw<n><sg>/see<vblex><past>$
[/-t9KC8OtnpLB]
[198FSOe4pTGp#0] ^from/from<pr>$ ^them/prpers<prn><obj><p3><mf><pl>$
[/198FSOe4pTGp]
[236rQatS7qjC#0] ^the/the<det><def><sp>$ ^cat/cat<n><sg>$ ^which/which<prn><itg><sp>/which<rel><an><mf><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$
[/236rQatS7qjC]
[2ZFYJyf4pf3N#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$
[/2ZFYJyf4pf3N]
[2x5sx0UccQyy#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$ ^saw/saw<n><sg>/see<vblex><past>$
[/2x5sx0UccQyy]
[42VNhIOw3Zxf#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$ ^saw/saw<n><sg>/see<vblex><past>$
[/42VNhIOw3Zxf]
[5Jxx4WCsEEEw#0] ^He/Prpers<prn><subj><p3><m><sg>$ ^hasn't/have<vbhaver><pres><p3><sg>+not<adv>$ ^done/do<vblex><pp>$ ^it/prpers<prn><subj><p3><nt><sg>/prpers<prn><obj><p3><nt><sg>$
[/5Jxx4WCsEEEw]
[5wyA_N0UpAQo#0] ^he/prpers<prn><subj><p3><m><sg>$ ^wrote/write<vblex><past>$ ^him/prpers<prn><obj><p3><m><sg>$ ^a/a<det><ind><sg>$ ^letter/letter<n><sg>$
[/5wyA_N0UpAQo]
[5xwAJ17TFCmV#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^they/prpers<prn><subj><p3><mf><pl>$ ^spoke/spoke<n><sg>/speak<vblex><past>$ ^with/with<pr>$
[/5xwAJ17TFCmV]
[5zX3Zb3fO8lq#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^books/book<n><pl>/book<vblex><pres><p3><sg>$
[/5zX3Zb3fO8lq]
[60s38mb4uW-5#0] ^with/with<pr>$ ^him/prpers<prn><obj><p3><m><sg>$
[/60s38mb4uW-5]
[64vSyuSjARZZ#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^she/prpers<prn><subj><p3><f><sg>$ ^spoke/spoke<n><sg>/speak<vblex><past>$ ^with/with<pr>$
[/64vSyuSjARZZ]
[6BbIOchO3ycD#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^books/book<n><pl>/book<vblex><pres><p3><sg>$
[/6BbIOchO3ycD]
[6Ope-qwWjjIj#0] ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^love/love<n><sg>/love<vblex><inf>/love<vblex><pres>$ ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$ ^saw/saw<n><sg>/see<vblex><past>$
[/6Ope-qwWjjIj]
[6Rck7j_3rAm7#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$ ^spoke/spoke<n><sg>/speak<vblex><past>$ ^with/with<pr>$
[/6Rck7j_3rAm7]
[6eWBXvV9DAsZ#0] ^He/Prpers<prn><subj><p3><m><sg>$ ^couldn't/can<vaux><past>+not<adv>$ ^smoke/smoke<n><sg>/smoke<vblex><inf>/smoke<vblex><pres>$
[/6eWBXvV9DAsZ]
[8yC_Blqwr76h#0] ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^love/love<n><sg>/love<vblex><inf>/love<vblex><pres>$ ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^who/who<prn><itg><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^books/book<n><pl>/book<vblex><pres><p3><sg>$
[/8yC_Blqwr76h]
[9Eoa8BZ7NGiq#0] ^although/although<cnjadv>$ ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^sing/sing<vblex><inf>/sing<vblex><pres>$ ^I'm/prpers<prn><subj><p1><mf><sg>+be<vbser><pres><p1><sg>$ ^sad/sad<adj><sint>$
[/9Eoa8BZ7NGiq]
[AAdhyZHnC30H#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^who/who<prn><itg><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^books/book<n><pl>/book<vblex><pres><p3><sg>$ ^love/love<n><sg>/love<vblex><inf>/love<vblex><pres>$ ^me/prpers<prn><obj><p1><mf><sg>$
[/AAdhyZHnC30H]
[BeN6Bun7t6Gl#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^who/who<prn><itg><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^book/book<n><sg>/book<vblex><inf>/book<vblex><pres>$ ^loves/love<n><pl>/love<vblex><pres><p3><sg>$ ^me/prpers<prn><obj><p1><mf><sg>$
[/BeN6Bun7t6Gl]
[Bg5HmfDhHTFG#0] ^the/the<det><def><sp>$ ^cat/cat<n><sg>$ ^which/which<prn><itg><sp>/which<rel><an><mf><sp>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$ ^saw/saw<n><sg>/see<vblex><past>$
[/Bg5HmfDhHTFG]
[DCZbC7dtYR5A#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^she/prpers<prn><subj><p3><f><sg>$ ^spoke/spoke<n><sg>/speak<vblex><past>$ ^to/to<pr>$
[/DCZbC7dtYR5A]
[FOyKsY4tBIQZ#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$ ^saw/saw<n><sg>/see<vblex><past>$ ^loves/love<n><pl>/love<vblex><pres><p3><sg>$ ^me/prpers<prn><obj><p1><mf><sg>$
[/FOyKsY4tBIQZ]
[G53gwRwC2kLQ#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$ ^saw/saw<n><sg>/see<vblex><past>$
[/G53gwRwC2kLQ]
[G7CG-KdTx5z6#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$ ^spoke/spoke<n><sg>/speak<vblex><past>$ ^to/to<pr>$
[/G7CG-KdTx5z6]
[Ghhih55Y_lJA#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^they/prpers<prn><subj><p3><mf><pl>$ ^spoke/spoke<n><sg>/speak<vblex><past>$ ^to/to<pr>$
[/Ghhih55Y_lJA]
[HRWxvTIxc7lc#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$
[/HRWxvTIxc7lc]
[Hr8pykwibj-Z#0] ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^hear/hear<vblex><inf>/hear<vblex><pres>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^he/prpers<prn><subj><p3><m><sg>$ ^will/will<n><sg>/will<vaux><pres>$ ^come/come<vblex><inf>/come<vblex><pres>/come<vblex><pp>$^./.<sent>$ ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^hear/hear<vblex><inf>/hear<vblex><pres>$ ^he/prpers<prn><subj><p3><m><sg>$ ^will/will<n><sg>/will<vaux><pres>$ ^come/come<vblex><inf>/come<vblex><pres>/come<vblex><pp>$^./.<sent>$
[/Hr8pykwibj-Z]
[InJ35sdnZ87n#0] ^the/the<det><def><sp>$ ^cats/cat<n><pl>$ ^which/which<prn><itg><sp>/which<rel><an><mf><sp>$ ^came/come<vblex><past>$
[/InJ35sdnZ87n]
[IzmMvIwegzvM#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^who/who<prn><itg><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^book/book<n><sg>/book<vblex><inf>/book<vblex><pres>$ ^love/love<n><sg>/love<vblex><inf>/love<vblex><pres>$ ^me/prpers<prn><obj><p1><mf><sg>$
[/IzmMvIwegzvM]
[JEWg3XS8QTLd#0] ^That/That<cnjsub>/That<det><dem><sg>/That<prn><tn><sg>/That<rel><an><mf><sp>$ ^means/mean<n><pl>/means<n><sg>/means<n><pl>/mean<vblex><pres><p3><sg>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^we/prpers<prn><subj><p1><mf><pl>$ ^can/can<n><sg>/can<vaux><pres>$ ^provide/provide<vblex><inf>/provide<vblex><pres>$ ^individual/individual<adj>/individual<n><sg>$ ^reports/report<n><pl>/report<vblex><pres><p3><sg>$^./.<sent>$ ^That/That<cnjsub>/That<det><dem><sg>/That<prn><tn><sg>/That<rel><an><mf><sp>$ ^means/mean<n><pl>/means<n><sg>/means<n><pl>/mean<vblex><pres><p3><sg>$ ^we/prpers<prn><subj><p1><mf><pl>$ ^can/can<n><sg>/can<vaux><pres>$ ^provide/provide<vblex><inf>/provide<vblex><pres>$ ^individual/individual<adj>/individual<n><sg>$ ^reports/report<n><pl>/report<vblex><pres><p3><sg>$^./.<sent>$
[/JEWg3XS8QTLd]
[K2D_r63Pn0AK#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$ ^saw/saw<n><sg>/see<vblex><past>$ ^loves/love<n><pl>/love<vblex><pres><p3><sg>$ ^me/prpers<prn><obj><p1><mf><sg>$
[/K2D_r63Pn0AK]
[KFSmA9I9m4We#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^book/book<n><sg>/book<vblex><inf>/book<vblex><pres>$
[/KFSmA9I9m4We]
[LeWw3hTteHKv#0] ^he/prpers<prn><subj><p3><m><sg>$ ^wrote/write<vblex><past>$ ^me/prpers<prn><obj><p1><mf><sg>$ ^a/a<det><ind><sg>$ ^letter/letter<n><sg>$
[/LeWw3hTteHKv]
[LfSr9OsjiYK6#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^them/prpers<prn><obj><p3><mf><pl>$
[/LfSr9OsjiYK6]
[Lh-Sy3sVu5Bv#0] ^he/prpers<prn><subj><p3><m><sg>$ ^wrote/write<vblex><past>$ ^you/prpers<prn><subj><p2><mf><sp>/prpers<prn><obj><p2><mf><sp>$ ^a/a<det><ind><sg>$ ^letter/letter<n><sg>$
[/Lh-Sy3sVu5Bv]
[LtoD4ueGxcQQ#0] ^can/can<n><sg>/can<vaux><pres>$ ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^see/see<vblex><inf>/see<vblex><pres>$^?/?<sent>$
[/LtoD4ueGxcQQ]
[MUjHPsJwyxMP#0] ^the/the<det><def><sp>$ ^cat/cat<n><sg>$ ^which/which<prn><itg><sp>/which<rel><an><mf><sp>$ ^came/come<vblex><past>$
[/MUjHPsJwyxMP]
[NQ_KG5shqo4Q#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^she/prpers<prn><subj><p3><f><sg>$ ^saw/saw<n><sg>/see<vblex><past>$
[/NQ_KG5shqo4Q]
[OJ6zvzppYGgK#0] ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^love/love<n><sg>/love<vblex><inf>/love<vblex><pres>$ ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$ ^saw/saw<n><sg>/see<vblex><past>$
[/OJ6zvzppYGgK]
[Q2YYFb4IBTu1#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^they/prpers<prn><subj><p3><mf><pl>$ ^spoke/spoke<n><sg>/speak<vblex><past>$ ^with/with<pr>$
[/Q2YYFb4IBTu1]
[QzyIZKcL0LGM#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^who/who<prn><itg><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^books/book<n><pl>/book<vblex><pres><p3><sg>$ ^loves/love<n><pl>/love<vblex><pres><p3><sg>$ ^me/prpers<prn><obj><p1><mf><sg>$
[/QzyIZKcL0LGM]
[RCqLxkk217L-#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$ ^saw/saw<n><sg>/see<vblex><past>$
[/RCqLxkk217L-]
[REh1-WYljbXV#0] ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^went/go<vblex><past>$ ^home/home<adv>/home<n><sg>$
[/REh1-WYljbXV]
[RxbewC4JLs16#0] ^give/give<vblex><inf>/give<vblex><pres>$ ^it/prpers<prn><subj><p3><nt><sg>/prpers<prn><obj><p3><nt><sg>$ ^to/to<pr>$ ^him/prpers<prn><obj><p3><m><sg>$
[/RxbewC4JLs16]
[T-eZqgiSSPMg#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^came/come<vblex><past>$
[/T-eZqgiSSPMg]
[TECbaKRgigMd#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^they/prpers<prn><subj><p3><mf><pl>$ ^saw/saw<n><sg>/see<vblex><past>$
[/TECbaKRgigMd]
[U_VN3rg29vct#0] ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^love/love<n><sg>/love<vblex><inf>/love<vblex><pres>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^friends/friend<n><pl>$
[/U_VN3rg29vct]
[VC50A_BG8vnF#0] ^He/Prpers<prn><subj><p3><m><sg>$ ^shan't/shall<vaux><inf>+not<adv>$ ^be/be<vbser><inf>$ ^a/a<det><ind><sg>$ ^doctor/doctor<n><sg>/doctor<vblex><inf>/doctor<vblex><pres>$
[/VC50A_BG8vnF]
[Vu7x8ojDrGkj#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$
[/Vu7x8ojDrGkj]
[WJ89HIuRMS74#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^them/prpers<prn><obj><p3><mf><pl>$
[/WJ89HIuRMS74]
[WrBrzWiY7YAl#0] ^He/Prpers<prn><subj><p3><m><sg>$ ^could/can<vaux><past>$ ^not/not<adv>$ ^smoke/smoke<n><sg>/smoke<vblex><inf>/smoke<vblex><pres>$
[/WrBrzWiY7YAl]
[XZkYJV-6N18T#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^they/prpers<prn><subj><p3><mf><pl>$ ^saw/saw<n><sg>/see<vblex><past>$
[/XZkYJV-6N18T]
[YMY9kW8DRHCB#0] ^He/Prpers<prn><subj><p3><m><sg>$ ^told/tell<vblex><past>/tell<vblex><pp>$ ^the/the<det><def><sp>$ ^children/child<n><pl>$ ^something/something<n><sg>/something<prn><tn><sg>$
[/YMY9kW8DRHCB]
[YcnG35bCHMyi#0] ^this/this<det><dem><sg>/this<prn><tn><sg>$ ^is/be<vbser><pres><p3><sg>$ ^for/for<cnjadv>/for<pr>$ ^us/prpers<prn><obj><p1><mf><pl>$
[/YcnG35bCHMyi]
[ZChHBjpzS8yk#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$ ^spoke/spoke<n><sg>/speak<vblex><past>$ ^to/to<pr>$
[/ZChHBjpzS8yk]
[_CbgqQZ4yEVc#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$ ^spoke/spoke<n><sg>/speak<vblex><past>$ ^with/with<pr>$
[/_CbgqQZ4yEVc]
[_IOBby9Tg2gU#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$ ^spoke/spoke<n><sg>/speak<vblex><past>$ ^to/to<pr>$
[/_IOBby9Tg2gU]
[_Y4_Mp1yaQDi#0] ^this/this<det><dem><sg>/this<prn><tn><sg>$ ^is/be<vbser><pres><p3><sg>$ ^for/for<cnjadv>/for<pr>$ ^them/prpers<prn><obj><p3><mf><pl>$
[/_Y4_Mp1yaQDi]
[_YazI85M7bA6#0] ^do/do<adv><itg>/do<vbdo><pres>/do<vblex><inf>/do<vblex><pres>$ ^you/prpers<prn><subj><p2><mf><sp>/prpers<prn><obj><p2><mf><sp>$ ^see/see<vblex><inf>/see<vblex><pres>$ ^him/prpers<prn><obj><p3><m><sg>$^?/?<sent>$
[/_YazI85M7bA6]
[_iVZQGD76rMm#0] ^the/the<det><def><sp>$ ^cats/cat<n><pl>$ ^which/which<prn><itg><sp>/which<rel><an><mf><sp>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$ ^saw/saw<n><sg>/see<vblex><past>$
[/_iVZQGD76rMm]
[b66x4fZBzRgi#0] ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^love/love<n><sg>/love<vblex><inf>/love<vblex><pres>$ ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^who/who<prn><itg><sp>$ ^came/come<vblex><past>$
[/b66x4fZBzRgi]
[bbJEWrRB8MFr#0] ^He/Prpers<prn><subj><p3><m><sg>$ ^has/have<vbhaver><pres><p3><sg>/have<vblex><pres><p3><sg>$ ^not/not<adv>$ ^done/do<vblex><pp>$ ^it/prpers<prn><subj><p3><nt><sg>/prpers<prn><obj><p3><nt><sg>$
[/bbJEWrRB8MFr]
[bg2nRnoRbG30#0] ^They/Prpers<prn><subj><p3><mf><pl>$ ^exploit/exploit<n><sg>/exploit<vblex><inf>/exploit<vblex><pres>$ ^all/all<predet><sp>/all<prn><tn><sp>$ ^of/of<pr>$ ^my/my<det><pos><sp>$ ^friends/friend<n><pl>$
[/bg2nRnoRbG30]
[bxYo4m-0aoR6#0] ^give/give<vblex><inf>/give<vblex><pres>$ ^it/prpers<prn><subj><p3><nt><sg>/prpers<prn><obj><p3><nt><sg>$ ^to/to<pr>$ ^it/prpers<prn><subj><p3><nt><sg>/prpers<prn><obj><p3><nt><sg>$
[/bxYo4m-0aoR6]
[byFjj7glCNUI#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^she/prpers<prn><subj><p3><f><sg>$ ^spoke/spoke<n><sg>/speak<vblex><past>$ ^with/with<pr>$
[/byFjj7glCNUI]
[cjIV8jVv2qbS#0] ^the/the<det><def><sp>$ ^cat/cat<n><sg>$ ^which/which<prn><itg><sp>/which<rel><an><mf><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$
[/cjIV8jVv2qbS]
[dS5mhBfijn6H#0] ^the/the<det><def><sp>$ ^cats/cat<n><pl>$ ^which/which<prn><itg><sp>/which<rel><an><mf><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$
[/dS5mhBfijn6H]
[ddNUAP8duWt7#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^her/her<det><pos><sp>/prpers<prn><obj><p3><f><sg>$
[/ddNUAP8duWt7]
[fEqIPzAx1cym#0] ^is/be<vbser><pres><p3><sg>$ ^the/the<det><def><sp>$ ^cat/cat<n><sg>$ ^big/big<adj><sint>$^?/?<sent>$
[/fEqIPzAx1cym]
[fHx0XKOYc-bb#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^she/prpers<prn><subj><p3><f><sg>$ ^spoke/spoke<n><sg>/speak<vblex><past>$ ^to/to<pr>$
[/fHx0XKOYc-bb]
[fseSEnc6iUxF#0] ^does/do<vbdo><pres><p3><sg>/do<vblex><pres><p3><sg>$ ^anyone/anyone<prn><tn><sg>$ ^understand/understand<vblex><inf>/understand<vblex><pres>$^?/?<sent>$
[/fseSEnc6iUxF]
[gDizo0tzKhK4#0] ^It/Prpers<prn><subj><p3><nt><sg>/Prpers<prn><obj><p3><nt><sg>$ ^does/do<vbdo><pres><p3><sg>/do<vblex><pres><p3><sg>$ ^work/work<n><sg>/work<vblex><inf>/work<vblex><pres>$^./.<sent>$
[/gDizo0tzKhK4]
[gNA7pqgXZn_t#0] ^the/the<det><def><sp>$ ^cats/cat<n><pl>$ ^which/which<prn><itg><sp>/which<rel><an><mf><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$
[/gNA7pqgXZn_t]
[h2guk4Enju-F#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$ ^saw/saw<n><sg>/see<vblex><past>$
[/h2guk4Enju-F]
[hG0wIJwoUOQz#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$ ^spoke/spoke<n><sg>/speak<vblex><past>$ ^with/with<pr>$
[/hG0wIJwoUOQz]
[hoXf0ToARR6D#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$ ^saw/saw<n><sg>/see<vblex><past>$
[/hoXf0ToARR6D]
[i_2YvJTH1XH9#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^she/prpers<prn><subj><p3><f><sg>$ ^saw/saw<n><sg>/see<vblex><past>$
[/i_2YvJTH1XH9]
[ieRGspochh8u#0] ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^love/love<n><sg>/love<vblex><inf>/love<vblex><pres>$ ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^who/who<prn><itg><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^book/book<n><sg>/book<vblex><inf>/book<vblex><pres>$
[/ieRGspochh8u]
[ih9A9cc65drB#0] ^without/without<pr>$ ^her/her<det><pos><sp>/prpers<prn><obj><p3><f><sg>$
[/ih9A9cc65drB]
[ipf4W_EZWJoG#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^she/prpers<prn><subj><p3><f><sg>$ ^saw/saw<n><sg>/see<vblex><past>$
[/ipf4W_EZWJoG]
[jE874Sd5zJgH#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^they/prpers<prn><subj><p3><mf><pl>$ ^saw/saw<n><sg>/see<vblex><past>$
[/jE874Sd5zJgH]
[jdeItB72femo#0] ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^love/love<n><sg>/love<vblex><inf>/love<vblex><pres>$ ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^who/who<prn><itg><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^book/book<n><sg>/book<vblex><inf>/book<vblex><pres>$
[/jdeItB72femo]
[kpDffYD09CMm#0] ^the/the<det><def><sp>$ ^cats/cat<n><pl>$ ^which/which<prn><itg><sp>/which<rel><an><mf><sp>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$ ^saw/saw<n><sg>/see<vblex><past>$
[/kpDffYD09CMm]
[lyD2_10H4uoe#0] ^he/prpers<prn><subj><p3><m><sg>$ ^wrote/write<vblex><past>$ ^us/prpers<prn><obj><p1><mf><pl>$ ^a/a<det><ind><sg>$ ^letter/letter<n><sg>$
[/lyD2_10H4uoe]
[msFcXuaRRh4S#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^they/prpers<prn><subj><p3><mf><pl>$ ^spoke/spoke<n><sg>/speak<vblex><past>$ ^to/to<pr>$
[/msFcXuaRRh4S]
[mxZZshoEsrlU#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^who/who<prn><itg><sp>$ ^came/come<vblex><past>$ ^love/love<n><sg>/love<vblex><inf>/love<vblex><pres>$ ^me/prpers<prn><obj><p1><mf><sg>$
[/mxZZshoEsrlU]
[nCMzB4CADJcP#0] ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^love/love<n><sg>/love<vblex><inf>/love<vblex><pres>$ ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^who/who<prn><itg><sp>$ ^came/come<vblex><past>$
[/nCMzB4CADJcP]
[nT14hQzXceck#0] ^does/do<vbdo><pres><p3><sg>/do<vblex><pres><p3><sg>$ ^he/prpers<prn><subj><p3><m><sg>$ ^have to/have<vaux><inf># to/have<vaux><pres># to$ ^go/go<vblex><inf>/go<vblex><pres>$^?/?<sent>$
[/nT14hQzXceck]
[noUm8hoJnU8J#0] ^video/video<n><sg>$ ^game/game<n><sg>$ ^design/design<n><sg>/design<vblex><inf>/design<vblex><pres>$
[/noUm8hoJnU8J]
[pZqmV2hvUCij#0] ^video/video<n><sg>$ ^game/game<n><sg>$ ^design/design<n><sg>/design<vblex><inf>/design<vblex><pres>$ ^industry/industry<n><sg>$
[/pZqmV2hvUCij]
[sR5EW9LuqSND#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$
[/sR5EW9LuqSND]
[tEs6t3ElmVXY#0] ^coffee/coffee<n><sg>$ ^break/break<n><sg>/break<vblex><inf>/break<vblex><pres>$ ^time/time<n><sg>$
[/tEs6t3ElmVXY]
[tFz51hbHXWfk#0] ^give/give<vblex><inf>/give<vblex><pres>$ ^it/prpers<prn><subj><p3><nt><sg>/prpers<prn><obj><p3><nt><sg>$ ^to/to<pr>$ ^her/her<det><pos><sp>/prpers<prn><obj><p3><f><sg>$
[/tFz51hbHXWfk]
[tSKHX_7tZOMG#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$ ^saw/saw<n><sg>/see<vblex><past>$
[/tSKHX_7tZOMG]
[tbXLAyRImF4F#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^her/her<det><pos><sp>/prpers<prn><obj><p3><f><sg>$
[/tbXLAyRImF4F]
[uE3WDCv0x7n0#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$ ^spoke/spoke<n><sg>/speak<vblex><past>$ ^to/to<pr>$
[/uE3WDCv0x7n0]
[uPuRDoer2JfI#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^book/book<n><sg>/book<vblex><inf>/book<vblex><pres>$
[/uPuRDoer2JfI]
[uRiiQq02fL9n#0] ^the/the<det><def><sp>$ ^cat/cat<n><sg>$ ^which/which<prn><itg><sp>/which<rel><an><mf><sp>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$ ^saw/saw<n><sg>/see<vblex><past>$
[/uRiiQq02fL9n]
[uXp-1ar6spNZ#0] ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^love/love<n><sg>/love<vblex><inf>/love<vblex><pres>$ ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$ ^saw/saw<n><sg>/see<vblex><past>$
[/uXp-1ar6spNZ]
[und-ha-sIvpV#0] ^is/be<vbser><pres><p3><sg>$ ^the/the<det><def><sp>$ ^cat/cat<n><sg>$ ^big/big<adj><sint>$^?/?<sent>$ ^Is/Be<vbser><pres><p3><sg>$ ^the/the<det><def><sp>$ ^cat/cat<n><sg>$ ^big/big<adj><sint>$^?/?<sent>$ ^Is/Be<vbser><pres><p3><sg>$ ^the/the<det><def><sp>$ ^cat/cat<n><sg>$ ^big/big<adj><sint>$^?/?<sent>$
[/und-ha-sIvpV]
[v6cCIFBa_AqX#0] ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^love/love<n><sg>/love<vblex><inf>/love<vblex><pres>$ ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^who/who<prn><itg><sp>$ ^saw/saw<n><sg>/see<vblex><past>$ ^the/the<det><def><sp>$ ^books/book<n><pl>/book<vblex><pres><p3><sg>$
[/v6cCIFBa_AqX]
[vQ5NP6F23wdq#0] ^give/give<vblex><inf>/give<vblex><pres>$ ^it/prpers<prn><subj><p3><nt><sg>/prpers<prn><obj><p3><nt><sg>$ ^to/to<pr>$ ^you/prpers<prn><subj><p2><mf><sp>/prpers<prn><obj><p2><mf><sp>$
[/vQ5NP6F23wdq]
[vgg04ezm2Ny0#0] ^He/Prpers<prn><subj><p3><m><sg>$ ^gave/give<vblex><past>$ ^the/the<det><def><sp>$ ^children/child<n><pl>$ ^a/a<det><ind><sg>$ ^present/present<adj>/present<n><sg>/present<vblex><inf>/present<vblex><pres>$
[/vgg04ezm2Ny0]
[vptnTaaKv_Ai#0] ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^came/come<vblex><past>$
[/vptnTaaKv_Ai]
[wkcitJCahydc#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$ ^spoke/spoke<n><sg>/speak<vblex><past>$ ^with/with<pr>$
[/wkcitJCahydc]
[x3k-KcpdxEbz#0] ^he/prpers<prn><subj><p3><m><sg>$ ^wrote/write<vblex><past>$ ^them/prpers<prn><obj><p3><mf><pl>$ ^a/a<det><ind><sg>$ ^letter/letter<n><sg>$
[/x3k-KcpdxEbz]
[x59j8RrD9loo#0] ^give/give<vblex><inf>/give<vblex><pres>$ ^it/prpers<prn><subj><p3><nt><sg>/prpers<prn><obj><p3><nt><sg>$ ^to/to<pr>$ ^me/prpers<prn><obj><p1><mf><sg>$
[/x59j8RrD9loo]
[xEM-vCiqC1nt#0] ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^think/think<vblex><inf>/think<vblex><pres>$ ^that/that<cnjsub>/that<det><dem><sg>/that<prn><tn><sg>/that<rel><an><mf><sp>$ ^he/prpers<prn><subj><p3><m><sg>$ ^is/be<vbser><pres><p3><sg>$ ^good/good<adv>/good<adj><sint>/good<n><sg>$^./.<sent>$ ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^think/think<vblex><inf>/think<vblex><pres>$ ^he/prpers<prn><subj><p3><m><sg>$ ^is/be<vbser><pres><p3><sg>$ ^good/good<adv>/good<adj><sint>/good<n><sg>$^./.<sent>$
[/xEM-vCiqC1nt]
[xfqJ9I0elha4#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$ ^saw/saw<n><sg>/see<vblex><past>$
[/xfqJ9I0elha4]
[xt-BCktkybR5#0] ^he/prpers<prn><subj><p3><m><sg>$ ^wrote/write<vblex><past>$ ^her/her<det><pos><sp>/prpers<prn><obj><p3><f><sg>$ ^a/a<det><ind><sg>$ ^letter/letter<n><sg>$
[/xt-BCktkybR5]
[ybd6l0x5pPkU#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^the/the<det><def><sp>$ ^girl/girl<n><sg>$ ^saw/saw<n><sg>/see<vblex><past>$ ^loves/love<n><pl>/love<vblex><pres><p3><sg>$ ^me/prpers<prn><obj><p1><mf><sg>$
[/ybd6l0x5pPkU]
[zRNi5YwFZhAD#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$ ^saw/saw<n><sg>/see<vblex><past>$ ^loves/love<n><pl>/love<vblex><pres><p3><sg>$ ^me/prpers<prn><obj><p1><mf><sg>$
[/zRNi5YwFZhAD]
[zhawEYZ8VSKf#0] ^the/the<det><def><sp>$ ^boy/boy<n><sg>$ ^who/who<prn><itg><sp>$ ^came/come<vblex><past>$ ^loves/love<n><pl>/love<vblex><pres><p3><sg>$ ^me/prpers<prn><obj><p1><mf><sg>$
[/zhawEYZ8VSKf]
[zkbUW3QpdElj#0] ^I/I<num><sg>/prpers<prn><subj><p1><mf><sg>$ ^love/love<n><sg>/love<vblex><inf>/love<vblex><pres>$ ^the/the<det><def><sp>$ ^boys/boy<n><pl>$ ^the/the<det><def><sp>$ ^girls/girl<n><pl>$ ^saw/saw<n><sg>/see<vblex><past>$
[/zkbUW3QpdElj]
[zppPELHg5hgb#0] ^do/do<adv><itg>/do<vbdo><pres>/do<vblex><inf>/do<vblex><pres>$ ^you/prpers<prn><subj><p2><mf><sp>/prpers<prn><obj><p2><mf><sp>$ ^understand/understand<vblex><inf>/understand<vblex><pres>$^?/?<sent>$
[/zppPELHg5hgb]
